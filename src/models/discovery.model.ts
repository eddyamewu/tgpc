export class DiscoveryModel {
  $key?: string;
  title: string;
  message: string;
  verse: string;
}

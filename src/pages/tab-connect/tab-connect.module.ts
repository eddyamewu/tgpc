import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabConnectPage } from './tab-connect';

@NgModule({
  declarations: [
    TabConnectPage,
  ],
  imports: [
    IonicPageModule.forChild(TabConnectPage),
  ],
})
export class TabConnectPageModule {}

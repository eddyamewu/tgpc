import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabMediaPage } from './tab-media';

@NgModule({
  declarations: [
    TabMediaPage,
  ],
  imports: [
    IonicPageModule.forChild(TabMediaPage),
  ],
})
export class TabMediaPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabGivingPage } from './tab-giving';

@NgModule({
  declarations: [
    TabGivingPage,
  ],
  imports: [
    IonicPageModule.forChild(TabGivingPage),
  ],
})
export class TabGivingPageModule {}

import {Component, ViewChild} from '@angular/core';
import {IonicPage, Nav, NavController, NavParams} from 'ionic-angular';

export interface PageInterface{
  title: string;
  pageName: string;
  tabComponent?: any;
  index?: number;
  icon: string;
}

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  rootPage = 'TabsPage';

  @ViewChild(Nav) nav: Nav;

  pages: PageInterface[] = [
    { title: 'Home', pageName: 'TabsPage', tabComponent: 'TabHomePage', icon:'home', index: 0 },
    { title: 'Media', pageName: 'TabsPage', tabComponent: 'TabMediaPage', icon:'play', index: 1 },
    { title: 'Connect', pageName: 'TabsPage', tabComponent: 'TabConnectPage', icon:'git-network', index: 2 },
    { title: 'Locate', pageName: 'TabsPage', tabComponent: 'TabLocatePage', icon:'compass', index: 3 },
    { title: 'Giving', pageName: 'TabsPage', tabComponent: 'TabGivingPage', icon:'body', index: 4 },

    // page without tabs
    { title: 'About', pageName: 'AboutPage', icon:'information-circle' },
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  openPage(page: PageInterface) {
    let params = {};
    let childNav = this.nav.getActiveChildNavs()[0];

    if(page.index) params = {tabIndex: page.index};

    if (childNav && page.index != undefined) {
      childNav.select(page.index)
    } else {
      this.nav.setRoot(page.pageName, params)
    }
  }

  isActive(page: PageInterface){
    let childNav = this.nav.getActiveChildNavs()[0];

    if (childNav){
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }

      return;
    }

    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return 'primary'
    }
  }
}

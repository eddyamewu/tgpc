import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabLocatePage } from './tab-locate';

@NgModule({
  declarations: [
    TabLocatePage,
  ],
  imports: [
    IonicPageModule.forChild(TabLocatePage),
  ],
})
export class TabLocatePageModule {}

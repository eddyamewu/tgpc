import {Component, Input} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomeService } from '../../services/home.service';
import { Devotion } from '../../models/devotion.model';

@IonicPage()
@Component({
  selector: 'page-tab-home',
  templateUrl: 'tab-home.html',

})
export class TabHomePage {
  devotions: Devotion[];

  @Input("header") header :HTMLElement;
  selectedSegment: string = "news";
  constructor(
    private _homeService: HomeService,
    public navCtrl: NavController,
    public navParams: NavParams
  ) { }

  private getDevotions() {
    this._homeService.getDevotions().subscribe(devotions => {
      this.devotions = devotions;
      console.log(this.devotions)
    });
  }

  ionViewDidLoad() {
    this.getDevotions();
    console.log('ionViewDidLoad TabHomePage');
  }

}
